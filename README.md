# Requirements
- Docker

# Running the service

## Spin up containers
```
docker-compose up -d
```

## Install dependencies & execute migrations
```
docker exec -it {php_container} sh -c "composer install && bin/console doctrine:migrations:migrate -n"
```

Create markdown page from Admin: http://localhost/admin 

Access markdown page via: http://localhost/{uri}

## Tests (Functional PHPUnit & Unit with PhpSpec)
```
docker exec -it mdown_php-fpm_1 sh -c "APP_ENV=test bin/phpunit && bin/phpspec run"
```