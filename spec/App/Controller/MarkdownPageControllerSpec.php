<?php

namespace spec\App\Controller;

use App\Controller\MarkdownPageController;
use App\Entity\MarkdownPage;
use App\MarkdownParser\Parsedown;
use PhpSpec\ObjectBehavior;
use Symfony\Component\HttpFoundation\Response;

class MarkdownPageControllerSpec extends ObjectBehavior
{
	function it_is_initializable()
	{
		$this->shouldHaveType(MarkdownPageController::class);
	}

	function it_returns_markdown_html(
		Response $response,
		MarkdownPage $markdownPage,
		Parsedown $parsedown
	) {
		$markdownPage->getContent()->willReturn('whatever');
		$parsedown->text('whatever')->willReturn('html_response');
		$response->setContent('html_response')->shouldBeCalled()->willReturn($response);
		$this->show($markdownPage, $parsedown, $response)->shouldBe($response);
	}
}
