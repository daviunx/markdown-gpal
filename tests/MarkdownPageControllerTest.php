<?php

namespace App\Tests;

use App\DataFixtures\MarkdownPageFixtures;
use PHPUnit\Framework\Constraint\Constraint;

class MarkdownPageControllerTest extends FixtureAwareTestCase
{
	private $client;

	public function setUp(): void
	{
		$this->client = static::createClient();

		// Base fixture for all tests
		$this->addFixture(new MarkdownPageFixtures());
		$this->executeFixtures();
	}

	public function testSuccessResponse()
	{
		$this->client->request('GET', '/whatever-is-perfect');

		$this->assertResponseIsSuccessful();
		$this->assertEquals(file_get_contents(__DIR__ . '/example.html'), $this->client->getResponse()->getContent());
	}

	public function testNotFoundResponse()
	{
		$this->client->request('GET', '/whatever-is-not-perfect');

		$this->assertResponseStatusCodeSame(404);
	}
}
