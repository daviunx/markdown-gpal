<?php

namespace App\Controller\Admin;

use App\Entity\MarkdownPage;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractDashboardController
{
	#[Route('/admin', name: 'admin')]
	public function index(): Response
	{
		// redirect to some CRUD controller
		$routeBuilder = $this->get(AdminUrlGenerator::class);

		return $this->redirect($routeBuilder->setController(MarkdownPageCrudController::class)->generateUrl());

	}

	public function configureDashboard(): Dashboard
	{
		return Dashboard::new()
			// the name visible to end users
			->setTitle('Geopal Corp.')
			// you can include HTML contents too (e.g. to link to an image)
			->setTitle('<img src="https://www.geopal.com/sites/all/themes/base_theme/dist/img/logo.png" width="110px">')

			// the path defined in this method is passed to the Twig asset() function
			->setFaviconPath('favicon.svg')

			// the domain used by default is 'messages'
			->setTranslationDomain('en')

			// there's no need to define the "text direction" explicitly because
			// its default value is inferred dynamically from the user locale
			->setTextDirection('ltr')

			// set this option if you prefer the page content to span the entire
			// browser width, instead of the default design which sets a max width
			->renderContentMaximized()

			// set this option if you prefer the sidebar (which contains the main menu)
			// to be displayed as a narrow column instead of the default expanded design
			->renderSidebarMinimized()

			// by default, all backend URLs include a signature hash. If a user changes any
			// query parameter (to "hack" the backend) the signature won't match and EasyAdmin
			// triggers an error. If this causes any issue in your backend, call this method
			// to disable this feature and remove all URL signature checks
			->disableUrlSignatures();

	}

	public function configureMenuItems(): iterable
	{
		yield MenuItem::linkToCrud('Page', 'fas fa-list', MarkdownPage::class);
	}
}
