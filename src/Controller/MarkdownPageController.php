<?php

namespace App\Controller;

use App\Entity\MarkdownPage;
use App\MarkdownParser\Parsedown;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MarkdownPageController extends AbstractController
{
	/**
	 * @Route("/{uri}", name="page_show")
	 */
	public function show(MarkdownPage $page, Parsedown $parsedown, Response $response): Response
	{
		return $response->setContent($parsedown->text($page->getContent()));
	}
}
