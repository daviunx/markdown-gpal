<?php

namespace App\DataFixtures;

use App\Entity\MarkdownPage;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class MarkdownPageFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
         $page = new MarkdownPage();
         $page
			 ->setUri('whatever-is-perfect')
			 ->setContent(
			 	file_get_contents(__DIR__.'/../../tests/example.md')
			 );

         $manager->persist($page);

        $manager->flush();
    }
}
